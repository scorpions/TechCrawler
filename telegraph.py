import requests, json
class telegraph:
    def CreateAccount(short_name, author_name):
        re = requests.get(url="https://api.telegra.ph/createAccount?short_name="+short_name+"&author_name="+author_name)
        jsn = re.json()
        if jsn['ok'] == 'True' or 'true':
            print("Account Created. short name:"+jsn['result']['short_name']+"\nauthor name:"+jsn['result']['author_name'])
            token = jsn['result']['access_token']
            return token
        else:
            print("Something went wrong...")
    def CreatePage(author_name,access_token,title,content):
        re2 = requests.post('https://api.telegra.ph/createPage', data=json.dumps({'access_token': access_token, 'title': title, 'author_name': author_name, 'content': [{'tag': 'p', 'children': [content]}], 'return_content': 'true'}).encode('utf-8'))
        #jsn2 = json.loads(re2.text)
        print(re2.text)
        #return jsn2['result']['url']
    def EditPage(access_token,page_url,title,author_name,content):
        re3 = requests.get('https://api.telegra.ph/editPage/'+page_url+'?access_token='+access_token+'&title='+title+'&author_name='+author_name+'&content=[{"tag":"p", "children:"'+'['+content+']'+'}]')
        jsn3 = re3.json()
        return jsn3['result']['url']
