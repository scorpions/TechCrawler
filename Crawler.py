#!/bin/python3

import requests, redis
from telegraph import telegraph
from bs4 import BeautifulSoup
r = redis.StrictRedis(host='localhost', port=6379, db=0)
bottoken = '391690352:AAGfPVEzQUjvg3L3bfsiYLpphug-95GMMiw'

def getlast(surl):
    page = requests.get(url=surl)
    soup = BeautifulSoup(page.text, 'html5lib')
    link = soup.find_all('a', class_="page-link")
    subjects = []
    for title in link:
        subjects.append({"title": title.text, "link": title.get('href')})
    return subjects
    
def get_content(url):
    page = requests.get(url=url).text
    soup = BeautifulSoup(page,"html5lib")
    content = soup.find_all('div', dir="ltr")
    text = str(content[0].get_text()).replace('"', '\\"')
    return text
def send_msg(dest, msg):
    global bottoken
    sndmsg = requests.get(url="https://api.telegram.org/bot{0}/sendMessage?chat_id={1}&text={2}".format(bottoken, dest, msg))
    if "error" not in sndmsg.text:
        return True
    else:
        return False
last = getlast('http://thehackernews.com')[0]
text = get_content(last['link'])
if r.lindex('thehackernews', 0) != last['link']:
    print(telegraph.CreatePage('TechCrawler', 'cc10f5e8a110a468f8a3a0ef7fd5b2b54ff8c68d2276113b30757cb906c3', last['title'], 'test'))
    #r.lpsuh('thehackernews', last['title'])